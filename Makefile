CXX=g++
CXXFLAGS=-std=c++11 -Wall -pedantic -g
VPATH=src
LD=g++
SRC=src
BIN=bin
LDFLAGS=-g -Wall -pedantic

BINARY=pavliad1_fml
SRC_FILES=$(wildcard $(SRC)/*.cpp)
OBJ_FILES=$(patsubst $(SRC)/%.cpp,%.o,$(SRC_FILES))


all: compile

run: compile
	.$(BIN)/$(BINARY)

compile: $(OBJ_FILES)
	$(LD) $(LDFLAGS) $^ -o $(BIN)/$(BINARY)
	
%.o: $(SRC)/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

clean: clean_binary
	rm -f *.o main

clean_binary:
	rm -f $(BIN)/$(BINARY)