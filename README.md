# FML_AST_interpreter

This is work in progress, what is missing is: parse ast from json (lol) (as of 4.3.2020 23:22)

Updated 5.3.2020 13.47

Finished json parsing.
---------------------------
Update 8.3.2020

Finished implementation of everything. It's important to note that there might be some bugs/behavior that can be different from normal fml.

I didn't bother doing anything efectively and didn't free pretty much any memory that i allocated since it's something that would just make the code less readable and doesn't really have much purpose in "warmup" project (could be probably easily fixed with share_ptr). As example of something i'd change is that ENull should be singleton.

Compilation:
Theres simple Makefile, so calling "make" should be enough to compile.

Testing:
in folder "tests" theres few programs i used for testing. Theres also simple script which takes fml source file as an argument, runs "fml parse --format=json" and runs my compiled program with it's output as standard input.

---------------------------
Update 12.3.2020

Added testing1 and testing2 with bugfixes, all tests pass.