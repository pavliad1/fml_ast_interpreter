#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <sstream>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"

using namespace std;

class RuntimeException : public exception {
public:
    RuntimeException(const string &errMsg) : m_Message(errMsg) {}

    virtual const char *what() const throw() {
        return m_Message.c_str();
    }

private:
    string m_Message;
};

class Environment;

struct EVal {
    virtual string ToString() = 0;

    virtual bool IsTrue() { return false; }

    virtual bool IsFunction() { return false; }

    virtual bool IsInteger() { return false; }

    virtual bool IsBoolean() { return false; }

    virtual bool IsNull() { return false; }

    virtual bool IsArray() { return false; }

    virtual bool IsObject() { return false; }

    virtual EVal *Copy() = 0;

    virtual EVal *CallMethod(const string &name, const vector<EVal *> args, Environment *env) {
        throw RuntimeException("Call method for " + ToString() + " not implemented.");
    }
};

struct EBoolean : public EVal {
    EBoolean(bool val) : m_Value(val) {}

    string ToString() override { return m_Value ? "true" : "false"; }

    bool IsTrue() override { return m_Value; }

    bool IsBoolean() override { return true; }

    EVal *Copy() override {
        return new EBoolean(m_Value);
    }

    EVal *CallMethod(const string &name, const vector<EVal *> args, Environment *env) override {
        if (args.size() != 1 || !args[0]->IsBoolean())
            throw RuntimeException("Method \"" + name + "\" for EBoolean requires 1 argument of EBoolean type.");

        EBoolean *other = (EBoolean *) args[0];
        if (name == "==") return new EBoolean(m_Value == other->m_Value);
        if (name == "!=") return new EBoolean(m_Value != other->m_Value);
        if (name == "&") return new EBoolean(m_Value && other->m_Value);
        if (name == "|") return new EBoolean(m_Value || other->m_Value);

        throw RuntimeException("Method \"" + name + "\" for EInteger is not implemented.");
    }

    bool m_Value;
};

struct EInteger : public EVal {
    EInteger(int val) : m_Value(val) {}

    string ToString() override { return to_string(m_Value); }

    bool IsTrue() override { return true; }

    bool IsInteger() override { return true; }

    EVal *Copy() override {
        return new EInteger(m_Value);
    }

    EVal *CallMethod(const string &name, const vector<EVal *> args, Environment *env) override {
        if (args.size() != 1 || !args[0]->IsInteger())
            throw RuntimeException("Method \"" + name + "\" for EInteger requires 1 argument of EInteger type.");

        EInteger *other = (EInteger *) args[0];
        if (name == "+") return new EInteger(m_Value + other->m_Value);
        if (name == "-") return new EInteger(m_Value - other->m_Value);
        if (name == "*") return new EInteger(m_Value * other->m_Value);
        if (name == "/") return new EInteger(m_Value / other->m_Value);
        if (name == "%") return new EInteger(m_Value % other->m_Value);
        if (name == "==") return new EBoolean(m_Value == other->m_Value);
        if (name == "!=") return new EBoolean(m_Value != other->m_Value);
        if (name == "<") return new EBoolean(m_Value < other->m_Value);
        if (name == "<=") return new EBoolean(m_Value <= other->m_Value);
        if (name == ">") return new EBoolean(m_Value > other->m_Value);
        if (name == ">=") return new EBoolean(m_Value >= other->m_Value);

        throw RuntimeException("Method \"" + name + "\" for EInteger is not implemented.");
    }

    int m_Value;
};

struct ENull : public EVal {
    string ToString() override { return "null"; }

    bool IsNull() override { return true; }

    EVal *Copy() override {
        return new ENull;
    }

    EVal *CallMethod(const string &name, const vector<EVal *> args, Environment *env) override {
        if (args.size() != 1)
            throw RuntimeException("Method \"" + name + "\" for ENull requires 1 argument.");

        EInteger *other = (EInteger *) args[0];
        if (name == "==") return new EBoolean(other->IsNull());
        if (name == "!=") return new EBoolean(!other->IsNull());

        throw RuntimeException("Method \"" + name + "\" for ENull is not implemented.");
    }

};

class Function;

struct EFunction : public EVal {
    EFunction(Function *func) : m_Function(func) {}

    string ToString() override { return "somefn"; }

    bool IsFunction() override { return true; }

    EVal *Copy() override {
        return new EFunction(m_Function);
    }

    Function *m_Function;
};

struct EArray : public EVal {
    EArray(size_t size, EVal *defaultValue) : m_Size(size) {
        m_Data.resize(size);
        for (size_t i = 0; i < size; i++)
            m_Data[i] = defaultValue->Copy();
    }

    EArray(size_t size, const vector<EVal*>& defaultValues) : m_Size(size) {
        m_Data.resize(size);
        for (size_t i = 0; i < size; i++)
            m_Data[i] = defaultValues[i]->Copy();
    }

    EArray(const EArray &other) : m_Size(other.m_Size) {
        for (size_t i = 0; i < m_Size; i++)
            m_Data[i] = other.m_Data[i]->Copy();
    }

    string ToString() override {
        stringstream s;
        s << "[";
        /*for (const auto &i : m_Data)
            s << i->ToString() << ", ";
        s.seekp(-1, std::ios_base::end);
        s << "]";*/
        int l = (int)m_Data.size();
        for (int i = 0; i < l; i++) {
            s << m_Data[i]->ToString();
            if (i < l - 1)
                s << ", ";
        }
        s << "]";
        //return "Array(" + to_string(m_Size) + ") " + s.str();
        return s.str();
    }

    bool IsArray() override { return true; }

    EVal *Copy() override {
        return new EArray(*this);
    }

    EVal *Get(int idx) {
        if (idx < 0 || idx >= m_Size)
            throw RuntimeException("Index is out of bounds.");

        return m_Data[idx];
    }

    void Set(int idx, EVal *value) {
        if (idx < 0 || idx >= m_Size)
            throw RuntimeException("Index is out of bounds.");

        m_Data[idx] = value;
    }

    size_t m_Size;
    vector<EVal *> m_Data;
};

struct EObject : public EVal {
    EObject(EVal *extends, const map<string, EVal *> members) : m_Extends(extends), m_Members(members) {
    }

    EObject(const EObject &other) {
        m_Extends = other.m_Extends->Copy();
        for (const auto &m : other.m_Members)
            m_Members.insert({m.first, m.second->Copy()});
    }

    string ToString() override {
        stringstream s;
        s << "[";
        for (const auto &i : m_Members)
            s << i.second->ToString() << ",";
        s.seekp(-1, std::ios_base::end);
        s << "]";
        return "Object(extends: " + m_Extends->ToString() + ", members: " + s.str() + ")";
    }

    bool IsTrue() { return true; }

    bool IsObject() override { return true; }

    EVal *Copy() override {
        return new EObject(*this);
    }

    EVal *AccessField(const string &field) {
        const auto &v = m_Members.find(field);

        if (v == m_Members.end())
            throw RuntimeException("Object has no field \"" + field + "\".");

        return v->second;

    }

    EVal *AssignField(const string &field, EVal *value) {
        const auto &v = m_Members.find(field);

        if (v == m_Members.end())
            throw RuntimeException("Object has no field \"" + field + "\".");

        v->second = value;
        return v->second;
    }
/*
    EVal *CallMethod(const string &name, const vector<EVal *> args, Environment *env) override {
        auto method = FindMethod(name);

        // TODO osetrit +,-,.. cally na zakladnich typech
        throw RuntimeException("CALL METHOD ON OBJ!");
    }*/

    pair<EVal *, EFunction *> FindMethod(const string &name) {
        const auto &f = m_Members.find(name);

        if (f == m_Members.end() || !f->second->IsFunction()) {
            if (m_Extends->IsObject())
                return ((EObject *) m_Extends)->FindMethod(name);
            throw RuntimeException("Object has no method \"" + name + "\".");
        }

        return make_pair(this, (EFunction *) f->second);
    }

    EVal *m_Extends;
    map<string, EVal *> m_Members;
};

class Environment {
public:
    Environment() : m_Parent(nullptr) {}

    Environment(Environment *parent) : m_Parent(parent) {}

    void print() {
        for (const auto &it : m_Data) {
            cout << "\"" << it.first << "\" = " << it.second->ToString() << endl;
        }
    }

    EVal *Find(const string &varName) {
        const auto &v = m_Data.find(varName);

        if (v != m_Data.end())
            return v->second;

        if (m_Parent)
            return m_Parent->Find(varName);

        return nullptr;
    }

    EVal *DeclareVariable(const string &varName, EVal *value) {
        const auto &v = m_Data.find(varName);

        if (v != m_Data.end())
            return nullptr;

        m_Data.insert(make_pair(varName, value));
        return value;
    }

    EVal *AssignVariable(const string &varName, EVal *value) {
        auto v = m_Data.find(varName);

        if (v != m_Data.end()) {
            v->second = value;
            return value;
        }

        if (m_Parent)
            return m_Parent->AssignVariable(varName, value);

        return nullptr;
    }

    Environment *GetTopLevelEnviroment() {
        auto env = this;
        while (env->m_Parent != nullptr)
            env = env->m_Parent;

        return env;
    }

    map<string, EVal *> GetData() const { return m_Data; }

private:
    Environment *m_Parent;
    map<string, EVal *> m_Data;
};

//  ----------------------------------------------------------------------------------------

struct Expression {
    virtual EVal *Evaluate(Environment *env) = 0;
};

struct Integer : public Expression {
    Integer(int val) : m_Val(val) {}

    EVal *Evaluate(Environment *env) {
        return new EInteger(m_Val);
    }


    int m_Val;
};

struct Boolean : public Expression {
    Boolean(bool val) : m_Val(val) {}

    EVal *Evaluate(Environment *env) {
        return new EBoolean(m_Val);
    }

    bool m_Val;
};

struct Null : public Expression {

    EVal *Evaluate(Environment *env) {
        return new ENull;
    }
};

struct Array : public Expression {
    Array(Expression *size, Expression *value) : m_Size(size), m_Value(value) {}

    EVal *Evaluate(Environment *env) {
        auto size = m_Size->Evaluate(env);
        //auto value = m_Value->Evaluate(env);

        EInteger *s = (EInteger *) size;
        if (!size->IsInteger() || s->m_Value <= 0)
            throw RuntimeException("Array requires positive EInteger size");

        vector<EVal*> values;
        for (int i = 0; i < s->m_Value; i++)
            values.push_back(m_Value->Evaluate(env));

        return new EArray(s->m_Value, values);
    }

    Expression *m_Size;
    Expression *m_Value;
};

struct AccessArray : public Expression {
    AccessArray(Expression *array, Expression *index) : m_Array(array), m_Index(index) {}

    EVal *Evaluate(Environment *env) {
        auto array = m_Array->Evaluate(env);
        auto index = m_Index->Evaluate(env);

        if (!array->IsArray())
            throw RuntimeException(array->ToString() + " is not array.");

        if (!index->IsInteger())
            throw RuntimeException("Index is not EInteger.");

        EArray *arr = (EArray *) array;
        EInteger *idx = (EInteger *) index;

        return arr->Get(idx->m_Value);
    }

    Expression *m_Array;
    Expression *m_Index;
};

struct AssignArray : public Expression {
    AssignArray(Expression *array, Expression *index, Expression *value) : m_Array(array), m_Index(index),
                                                                           m_Value(value) {}

    EVal *Evaluate(Environment *env) {
        auto array = m_Array->Evaluate(env);
        auto index = m_Index->Evaluate(env);
        auto value = m_Value->Evaluate(env);

        if (!array->IsArray())
            throw RuntimeException(array->ToString() + " is not array.");

        if (!index->IsInteger())
            throw RuntimeException("Index is not EInteger.");

        EArray *arr = (EArray *) array;
        EInteger *idx = (EInteger *) index;

        arr->Set(idx->m_Value, value->Copy());
        return new ENull;
    }

    Expression *m_Array;
    Expression *m_Index;
    Expression *m_Value;
};

struct Variable : public Expression {
    Variable(const string &name, Expression *val) : m_Name(name), m_Val(val) {}

    EVal *Evaluate(Environment *env) {
        EVal *rhs = m_Val->Evaluate(env);
        if (!env->DeclareVariable(m_Name, rhs))
            throw RuntimeException("Variable \"" + m_Name + "\" was already declared.");

        return rhs;
    }

    string m_Name;
    Expression *m_Val;
};

struct AccessVariable : public Expression {
    AccessVariable(const string &name) : m_Name(name) {}

    EVal *Evaluate(Environment *env) {
        auto val = env->Find(m_Name);
        if (!val)
            throw RuntimeException("Variable \"" + m_Name + "\" was not declared in this scope.");
        return val;
    }

    string m_Name;
};

struct AssignVariable : public Expression {
    AssignVariable(const string &name, Expression *val) : m_Name(name), m_Val(val) {}

    EVal *Evaluate(Environment *env) {
        EVal *rhs = m_Val->Evaluate(env);
        auto val = env->AssignVariable(m_Name, rhs);

        if (!val)
            throw RuntimeException("Variable \"" + m_Name + "\" was not declared in this scope.");

        return rhs;
    }

    string m_Name;
    Expression *m_Val;
};

struct Block : public Expression {
    Block(const vector<Expression *> expr) : m_Expressions(expr) {}

    EVal *Evaluate(Environment *env) {
        Environment *childEnv = new Environment(env);

        EVal *lastExpr = nullptr;
        for (const auto &expr : m_Expressions) {
            lastExpr = expr->Evaluate(childEnv);
        }

        return lastExpr ? lastExpr : new ENull;
    }

    vector<Expression *> m_Expressions;
};

struct Top : public Block {
    Top(const vector<Expression *> expr) : Block(expr) {}

    EVal *Evaluate(Environment *env) {

        EVal *lastExpr = nullptr;
        for (const auto &expr : m_Expressions) {
            lastExpr = expr->Evaluate(env);
        }

        return lastExpr ? lastExpr : new ENull;
    }
};

struct Conditional : public Expression {
    Conditional(Expression *cond, Expression *tb, Expression *fb) : m_Condition(cond), m_TrueBranch(tb),
                                                                    m_FalseBranch(fb) {}

    EVal *Evaluate(Environment *env) {
        auto condEval = m_Condition->Evaluate(env);

        if (condEval->IsTrue())
            return m_TrueBranch->Evaluate(env);
        else
            return m_FalseBranch->Evaluate(env);
    }

    Expression *m_Condition;
    Expression *m_TrueBranch;
    Expression *m_FalseBranch;
};

struct Function : public Expression {
    Function(const string &name, const vector<string> &paramNames, Expression *body) : m_Name(name),
                                                                                       m_ParamNames(paramNames),
                                                                                       m_Body(body), m_IsMethod(false) {}

    EVal *Evaluate(Environment *env) {
        if (env->Find(m_Name))
            throw RuntimeException("Function \"" + m_Name + "\" was already declared.");

        EFunction *fn = new EFunction(this);
        env->DeclareVariable(m_Name, fn);

        return new ENull;
    }

    void AddParam(const string &paramName) {
        if (paramName == "this" && !m_IsMethod) {
            m_ParamNames.push_back(paramName);
            m_IsMethod = true;
        }
    }

    string m_Name;
    vector<string> m_ParamNames;
    Expression *m_Body;
    bool  m_IsMethod;
};

struct CallFunction : public Expression {
    CallFunction(const string &name, const vector<Expression *> args) : m_Name(name), m_Args(args) {}

    EVal *Evaluate(Environment *env) {

        auto *fn = env->Find(m_Name);

        if (!fn || !fn->IsFunction())
            throw RuntimeException("Function \"" + m_Name + "\" doesn't exist.");

        Function *f = ((EFunction *) fn)->m_Function;
        if (f->m_ParamNames.size() != m_Args.size())
            throw RuntimeException("Invalid number of arguments for \"" + m_Name + "\".");

        vector<EVal *> evalArgs;
        for (const auto &i : m_Args)
            evalArgs.push_back(i->Evaluate(env));

        Environment *childEnv = new Environment(env->GetTopLevelEnviroment());
        for (int i = 0; i < evalArgs.size(); i++) {
            if (!childEnv->DeclareVariable(f->m_ParamNames[i], evalArgs[i]))
                throw RuntimeException("Error initializing parameter in function \"" + m_Name + "\".");
        }

        return f->m_Body->Evaluate(childEnv);

    }

    string m_Name;
    vector<Expression *> m_Args;
};

struct CallMethod : public Expression {
    CallMethod(Expression *object, const string &name, const vector<Expression *> args) : m_Object(object),
                                                                                          m_Name(name), m_Args(args) {}

    EVal *Evaluate(Environment *env) {

        EVal *obj = m_Object->Evaluate(env);
        vector<EVal *> evalArgs;
        for (const auto &i : m_Args)
            evalArgs.push_back(i->Evaluate(env));

        // call on basic objects (integer, boolean, null, array)
        if (!obj->IsObject())
            return obj->CallMethod(m_Name, evalArgs, env);

        // call on object
        auto object = (EObject *) obj;
        auto objAndMethod = object->FindMethod(m_Name); // this throws if it doesn't find any appropriate

        // TODO: i assume here that it returned actual object and not (integer...) !
        evalArgs.push_back(objAndMethod.first); // push "this" as last argument

        Function *f = ((EFunction *) objAndMethod.second)->m_Function;
        if (f->m_ParamNames.size() != evalArgs.size())
            throw RuntimeException("Invalid number of arguments for method \"" + m_Name + "\" " +
                                  to_string(f->m_ParamNames.size()) + " != " + to_string(m_Args.size()) + ".");

        Environment *childEnv = new Environment(env->GetTopLevelEnviroment());
        for (int i = 0; i < evalArgs.size(); i++) {
            if (!childEnv->DeclareVariable(f->m_ParamNames[i], evalArgs[i]))
                throw RuntimeException("Error initializing parameter in method \"" + m_Name + "\".");
        }

        return f->m_Body->Evaluate(childEnv);
    }

    Expression *m_Object;
    string m_Name;
    vector<Expression *> m_Args;
};

struct Loop : public Expression {
    Loop(Expression *cond, Expression *body) : m_Condition(cond), m_Body(body) {}

    EVal *Evaluate(Environment *env) {

        while (m_Condition->Evaluate(env)->IsTrue())
            m_Body->Evaluate(env);

        return new ENull;
    }

    Expression *m_Condition;
    Expression *m_Body;
};

struct Print : public Expression {
    Print(const string &format, const vector<Expression *> args) : m_Format(format), m_Args(args) {}

    EVal *Evaluate(Environment *env) {

        auto format = replaceAll(m_Format, "\\n", "\n");
        stringstream stream;
        int pos = 0;
        for (const auto &c : format) {
            if (c == '~') {
                if (pos < m_Args.size()) {
                    auto argEval = m_Args[pos]->Evaluate(env);
                    stream << argEval->ToString();
                    pos++;
                }
            } else
                stream << c;
        }

        if (pos != m_Args.size())
            throw RuntimeException("Invalid Print format (argCnt != tildaCnt)");

        cout << stream.str();

        return new ENull;
    }

    // https://gist.github.com/GenesisFR/cceaf433d5b42dcdddecdddee0657292
    string replaceAll(string str, const string &from, const string &to) {
        size_t start_pos = 0;
        while ((start_pos = str.find(from, start_pos)) != string::npos) {
            str.replace(start_pos, from.length(), to);
            start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
        }
        return str;
    }

    string m_Format;
    vector<Expression *> m_Args;
};

struct Object : public Expression {

    Object(Expression *extends, const vector<Expression *> &members) : m_Extends(extends), m_Members(members) {}

    EVal *Evaluate(Environment *env) {

        auto extends = m_Extends->Evaluate(env);

        Environment *objectEnv = new Environment(env);
        for (const auto &m : m_Members)
            m->Evaluate(objectEnv);

        map<string, EVal *> members = objectEnv->GetData();
        for (auto &member : members) {
            if (member.second->IsFunction())
                ((EFunction *) member.second)->m_Function->AddParam("this");
        }

        return new EObject(extends, members);
    }

    Expression *m_Extends;
    vector<Expression *> m_Members;
};

struct AccessField : public Expression {

    AccessField(Expression *object, const string &field) : m_Object(object), m_Field(field) {}

    EVal *Evaluate(Environment *env) {
        auto object = m_Object->Evaluate(env);

        if (!object->IsObject())
            throw RuntimeException("Cannot access field \"" + m_Field + "\" of non-object.");

        return ((EObject *) object)->AccessField(m_Field);
    }

    Expression *m_Object;
    string m_Field;
};

struct AssignField : public Expression {

    AssignField(Expression *object, const string &field, Expression *value) : m_Object(object), m_Field(field),
                                                                              m_Value(value) {}

    EVal *Evaluate(Environment *env) {
        auto object = m_Object->Evaluate(env);
        auto value = m_Value->Evaluate(env);

        if (!object->IsObject())
            throw RuntimeException("Cannot assign field \"" + m_Field + "\" of non-object.");

        return ((EObject *) object)->AssignField(m_Field, value);
    }

    Expression *m_Object;
    string m_Field;
    Expression *m_Value;
};

Expression *createNode(rapidjson::Value::ConstMemberIterator itr) {

    const string &nodeName = itr->name.GetString();
    if (nodeName == "Top" || nodeName == "Block") {
        vector<Expression *> statements;
        for (auto &v : itr->value.GetArray()) {
            if (v.IsObject()) {
                auto obj = v.GetObject();
                for (rapidjson::Value::ConstMemberIterator itr2 = obj.MemberBegin(); itr2 != obj.MemberEnd(); ++itr2) {
                    statements.push_back(createNode(itr2));
                }
            } else
                statements.push_back(new Null);
        }
        if (nodeName == "Top")
            return new Top(statements);
        return new Block(statements);
    } else if (nodeName == "Function") {
        const auto &name = itr->value.GetObject()["name"].GetString();
        const auto &parameters = itr->value.GetObject()["parameters"].GetArray();
        const auto &body = itr->value.GetObject()["body"];
        vector<string> params;
        for (const auto &param : parameters)
            params.push_back(param.GetString());

        if (body.IsObject())
            return new Function(name, params, createNode(body.GetObject().MemberBegin()));
        return new Function(name, params, new Null);
    } else if (nodeName == "CallFunction") {
        const auto &name = itr->value.GetObject()["name"].GetString();
        const auto &arguments = itr->value.GetObject()["arguments"].GetArray();
        vector<Expression *> args;
        for (const auto &arg : arguments)
            args.push_back(createNode(arg.GetObject().MemberBegin()));
        return new CallFunction(name, args);
    } else if (nodeName == "CallMethod") {
        const auto &object = itr->value.GetObject()["object"];
        const auto &name = itr->value.GetObject()["name"].GetString();
        const auto &arguments = itr->value.GetObject()["arguments"].GetArray();

        Expression *obj = object.IsObject() ? createNode(object.MemberBegin()) : new Null;
        vector<Expression *> args;
        for (const auto &arg : arguments)
            args.push_back(arg.IsObject() ? createNode(arg.GetObject().MemberBegin()) : new Null);
        return new CallMethod(obj, name, args);
    } else if (nodeName == "Integer") {
        return new Integer(itr->value.GetInt());
    } else if (nodeName == "Boolean") {
        return new Boolean(itr->value.GetBool());
    } else if (nodeName == "Null") { // never used
        return new Null;
    } else if (nodeName == "Variable" || nodeName == "AssignVariable") {
        const auto &name = itr->value.GetObject()["name"].GetString();
        const auto &value = itr->value.GetObject()["value"];
        Expression *val;
        if (value.IsObject())
            val = createNode(value.GetObject().MemberBegin());
        else
            val = new Null;
        if (nodeName == "Variable")
            return new Variable(name, val);
        return new AssignVariable(name, val);
    } else if (nodeName == "AccessVariable") {
        const auto &name = itr->value.GetObject()["name"].GetString();
        return new AccessVariable(name);
    } else if (nodeName == "Print") {
        const auto &format = itr->value.GetObject()["format"].GetString();
        const auto &arguments = itr->value.GetObject()["arguments"].GetArray();
        vector<Expression *> args;
        for (const auto &arg : arguments) {
            if (arg.IsObject())
                args.push_back(createNode(arg.GetObject().MemberBegin()));
            else
                args.push_back(new Null);
        }
        return new Print(format, args);
    } else if (nodeName == "Loop") {
        const auto &condition = itr->value.GetObject()["condition"];
        const auto &body = itr->value.GetObject()["body"];
        Expression *cond = condition.IsObject() ? createNode(condition.GetObject().MemberBegin()) : new Null;
        Expression *bod = body.IsObject() ? createNode(body.GetObject().MemberBegin()) : new Null;
        return new Loop(cond, bod);
    } else if (nodeName == "Conditional") {
        const auto &condition = itr->value.GetObject()["condition"];
        const auto &consequent = itr->value.GetObject()["consequent"];
        const auto &alternative = itr->value.GetObject()["alternative"];
        Expression *cond = condition.IsObject() ? createNode(condition.GetObject().MemberBegin()) : new Null;
        Expression *cons = consequent.IsObject() ? createNode(consequent.GetObject().MemberBegin()) : new Null;
        Expression *alt = alternative.IsObject() ? createNode(alternative.GetObject().MemberBegin()) : new Null;
        return new Conditional(cond, cons, alt);
    } else if (nodeName == "Array") {
        const auto &size = itr->value.GetObject()["size"].GetObject();
        const auto &value = itr->value.GetObject()["value"];
        Expression *val = value.IsObject() ? createNode(value.GetObject().MemberBegin()) : new Null;
        return new Array(createNode(size.MemberBegin()), val);
    } else if (nodeName == "AccessArray") {
        const auto &array = itr->value.GetObject()["array"].GetObject();
        const auto &index = itr->value.GetObject()["index"];
        Expression *idx = index.IsObject() ? createNode(index.GetObject().MemberBegin()) : new Null;
        return new AccessArray(createNode(array.MemberBegin()), idx);
    } else if (nodeName == "AssignArray") {
        const auto &array = itr->value.GetObject()["array"].GetObject();
        const auto &index = itr->value.GetObject()["index"];
        const auto &value = itr->value.GetObject()["value"];
        Expression *val = value.IsObject() ? createNode(value.GetObject().MemberBegin()) : new Null;
        Expression *idx = index.IsObject() ? createNode(index.GetObject().MemberBegin()) : new Null;
        return new AssignArray(createNode(array.MemberBegin()), idx, val);
    } else if (nodeName == "Object") {
        const auto &extends = itr->value.GetObject()["extends"];
        const auto &members = itr->value.GetObject()["members"].GetArray();
        Expression *ext = extends.IsObject() ? createNode(extends.GetObject().MemberBegin()) : new Null;
        vector<Expression *> membersArr;
        for (const auto &i : members)
            membersArr.push_back(createNode(i.GetObject().MemberBegin()));

        return new Object(ext, membersArr);
    } else if (nodeName == "AccessField") {
        const auto &object = itr->value.GetObject()["object"];
        const auto &field = itr->value.GetObject()["field"].GetString();

        return new AccessField(object.IsObject() ? createNode(object.GetObject().MemberBegin()) : new Null, field);
    } else if (nodeName == "AssignField") {
        const auto &object = itr->value.GetObject()["object"];
        const auto &field = itr->value.GetObject()["field"].GetString();
        const auto &value = itr->value.GetObject()["value"];

        Expression *obj = object.IsObject() ? createNode(object.GetObject().MemberBegin()) : new Null;
        Expression *val = value.IsObject() ? createNode(value.GetObject().MemberBegin()) : new Null;

        return new AssignField(obj, field, val);
    }

    throw RuntimeException("Error parsing from JSON (THIS SHOULDN'T HAPPEN!!)");
}

Expression *parse() {

    string input;
    getline(cin, input);
    rapidjson::Document d;
    d.Parse(input.c_str());

    return createNode(d.MemberBegin());
}

int main() {

    Expression *ast = parse();

    Environment *env = new Environment;

    try {
        ast->Evaluate(env);
    } catch (RuntimeException &e) {
        cout << "Program ended unexpectedly.\n";
        cout << e.what() << endl;
    }

    return 0;
}
