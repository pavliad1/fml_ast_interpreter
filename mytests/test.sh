#bin/sh

bin="./bin/pavliad1_fml"
file="$1"

if [ $# -eq 0 ]
  then
    echo "File with FML source expected as argument."
	exit
fi

if [ -f "$file" ]; then
	fml parse --format=json "$1" |./$bin
else
	echo "Error file \"$file\" doesn't exist"
fi